package com.example.swaleha.riddhi;

public class Notify {

    public String status;
    public String sysTime;
    public String username;

    public Notify()
    {

    }

    public Notify(String status, String sysTime, String username) {
        this.status = status;
        this.sysTime = sysTime;
        this.username = username;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getSysTime() {
        return sysTime;
    }

    public void setSysTime(String sysTime) {
        this.sysTime = sysTime;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}
