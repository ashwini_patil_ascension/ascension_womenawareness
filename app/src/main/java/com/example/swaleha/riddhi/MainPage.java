package com.example.swaleha.riddhi;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class MainPage extends AppCompatActivity {

    FirebaseDatabase mFireData;
    DatabaseReference mdataref;
    ImageView img1, img2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_page);
        img1 = findViewById(R.id.img1);
        img2 = findViewById(R.id.img2);

        request();

        mFireData = FirebaseDatabase.getInstance();
//        mdataref = mFireData.getReference("Notifications");

        //Notify name=snapshot.child("Notifications").getValue(Notify.class);

        //Toast.makeText(getApplicationContext(),.getStatus(),Toast.LENGTH_LONG).show();
//        mdataref.addValueEventListener(new ValueEventListener() {
//            @Override
//            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
//                //DataPoint[] mDataPoint = new DataPoint[(int) dataSnapshot.getChildrenCount()];
//
//                for (DataSnapshot mDataSnapshot : dataSnapshot.getChildren()) {
//                    Notify mDataValue = mDataSnapshot.getValue(Notify.class);
//                    Toast.makeText(getApplicationContext(), mDataValue.getStatus(), Toast.LENGTH_LONG).show();
//                }
//
//            }
//
//            @Override
//            public void onCancelled(@NonNull DatabaseError databaseError) {
//
//            }
//        });
//
//                    String username = notify.getUsername();
//
//                    if(username.equals(notify.getUsername()) && notify.getStatus() == "0")
//                    {
//                        Toast.makeText(getApplicationContext(),""+notify.getStatus(),Toast.LENGTH_LONG).show();
//                    }


        img1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                startActivity(new Intent(getApplicationContext(), FirstVideo.class));
            }
        });

        img2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                startActivity(new Intent(getApplicationContext(), SecondActivity.class));

            }
        });
    }

    private void request() {

        AlertDialog.Builder adb = new AlertDialog.Builder(this, R.style.AlertDialogCustom);
        adb.setTitle("Can you help the women ??");
        adb.setIcon(android.R.drawable.ic_dialog_alert);
        adb.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                Toast.makeText(MainPage.this, "text",
                        Toast.LENGTH_SHORT).show();

                DatabaseReference dbref = mFireData.getReference("Notifications");
                // system time
                // status
                // userid
                dbref.child("status").setValue("1");


            }
        });
        adb.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                finish();
            }
        });
        adb.show();

    }

}


